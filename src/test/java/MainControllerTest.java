package tests;

import com.ikosmov.controller.MainController;
import com.ikosmov.dao.AirportDao;
import com.ikosmov.dao.FlightDao;
import com.ikosmov.dao.PassengerDao;
import com.ikosmov.dao.TicketDao;
import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Passenger;
import com.ikosmov.entity.Ticket;
import com.ikosmov.utils.HibernateConf;
import com.ikosmov.utils.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(locations = {"/resources/ikosmov-spring.xml"})
@WebAppConfiguration
public class MainControllerTest {
    @Autowired
    WebApplicationContext context;
    @Autowired
    AirportDao airportDao;
    @Autowired
    PassengerDao passengerDao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    TicketDao ticketDao;
    Ticket ticket1 = new Ticket(1, 1, 1);
    Ticket ticket2 = new Ticket(2, 2, 1);
    Ticket ticket3 = new Ticket(3, 3, 1);
    Ticket ticket4 = new Ticket(4, 4, 2);
    Passenger passenger1 = new Passenger(1, "John");
    Passenger passenger2 = new Passenger(2, "Andrew");
    Passenger passenger3 = new Passenger(3, "Michael");
    Passenger passenger5 = new Passenger(4, "DIma");
    Flight flight1 = new Flight(1, "Samara", "Moscow");
    Flight flight2 = new Flight(2, "Samara", "Saratov");

    @Test
    public void testTicketDao() throws Exception {
        MainController controller=context.getBean(MainController.class);
        controller.init();
        List<Ticket>list=Stream.of(ticket1,ticket2,ticket3,ticket4).collect(Collectors.toList());
        System.out.println("Get /tickets/");
        ticketDao.getTickets().forEach(System.out::println);
        Assert.assertEquals(list,ticketDao.getTickets());
        System.out.println("Get /tickets/1");
        Assert.assertEquals(ticket1,ticketDao.getTicket(1));
        Ticket ticket=new Ticket(5,1,2);
        System.out.println("Post /tickets/ /tickets/5");
        Assert.assertEquals(new Response("ok",ticket),ticketDao.postTicket(5,1,2));
        System.out.println("Delete /tickets/1");
        Assert.assertEquals(new Response("ok",ticket1),ticketDao.deleteTicket(1));
        System.out.println("Get /tickets/2/passenger");
        Assert.assertEquals(passenger2,ticketDao.getTicket(2).getPassenger());
        System.out.println("Get /tickets/2/flight");
        Assert.assertEquals(flight1,ticketDao.getTicket(2).getFlight());
    }

    @Test
    public void testFlightDao() {
        List<Flight>list=Stream.of(flight1,flight2).collect(Collectors.toList());
        System.out.println("Get /flights/");
        flightDao.getFlights().forEach(System.out::println);
        Assert.assertEquals(list,flightDao.getFlights());
        System.out.println("Get /flights/1");
        Assert.assertEquals(flight1,flightDao.getFlight(1));
        Flight flight=new Flight(3,"samara","london");
        System.out.println("Post /flights/ /flights/3");
        Assert.assertEquals(new Response("ok",flight),flightDao.postFlight(3,"samara","london"));
        System.out.println("Delete /flights/3");
        Assert.assertEquals(new Response("ok",flight),flightDao.deleteFlight(3));
        System.out.println("Get /flights/1/tickets");
        Assert.assertEquals(Stream.of(ticket2,ticket3).collect(Collectors.toList()),
                flightDao.getFlightTickets(1));
        System.out.println("Get /flights/1/passengers");
        Assert.assertEquals(Stream.of(passenger1,passenger2,passenger3).collect(Collectors.toSet()),
                flightDao.getFlightPassengers(1));
    }

    @Test
    public void testPassengersDao() {
        List<Passenger>list=Stream.of(passenger1,passenger2,passenger3,passenger5).collect(Collectors.toList());
        System.out.println("Get /passengers/");
        flightDao.getFlights().forEach(System.out::println);
        Assert.assertEquals(list,passengerDao.getPassengers());
        System.out.println("Get /passengers/1");
        Assert.assertEquals(passenger1,passengerDao.getPassenger(1));
        Passenger passenger=new Passenger(5,"guy");
        System.out.println("Post /passengers/ /passengers/5");
        Assert.assertEquals(new Response("ok",passenger),passengerDao.postPassenger(5,"guy"));
        System.out.println("Delete /passengers/3");
        Assert.assertEquals(new Response("ok",passenger),passengerDao.deletePassenger(5));
        System.out.println("Get /passengers/2/tickets");
        Assert.assertEquals(Stream.of(ticket2).collect(Collectors.toList()),
                passengerDao.getPassengersTickets(2));
        System.out.println("Get /passengers/1/flights");
        Assert.assertEquals(Stream.of(flight1).collect(Collectors.toSet()),
                passengerDao.getPassengersFlights(1));
    }
}
