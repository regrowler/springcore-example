package com.ikosmov.controller;

import com.ikosmov.dao.FlightDao;
import com.ikosmov.dao.PassengerDao;
import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Passenger;
import com.ikosmov.utils.Response;
import com.ikosmov.dao.TicketDao;
import com.ikosmov.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class TicketController {
    @Autowired
    TicketDao dao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    PassengerDao passengerDao;

    @RequestMapping("/tickets")
    public void getAll(HttpServletResponse response) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        dao.getTickets().forEach(writer::println);
    }
    @RequestMapping(value = "/tickets",method = RequestMethod.POST)
    public void add(HttpServletResponse response,
                    @RequestHeader int id,
                    @RequestHeader int flight,
                    @RequestHeader int passenger) throws IOException {
        PrintWriter writer=response.getWriter();
        writer.println(dao.postTicket(id,passenger,flight));
    }
    @RequestMapping("/tickets/{id}")
    public void getById(HttpServletResponse response, @PathVariable String id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        writer.println(dao.getTicket(Integer.valueOf(id)));
    }
    @RequestMapping(value = "/tickets/{id}",method = RequestMethod.POST)
    public void putTicket(HttpServletResponse response,
                          @PathVariable int id,
                          @RequestHeader int flight,
                          @RequestHeader int passenger) throws IOException {
        PrintWriter writer=response.getWriter();
        writer.println(dao.postTicket(id,passenger,flight));
    }
    @RequestMapping(value = "/tickets/{id}",method = RequestMethod.DELETE)
    public void deleteTicket(HttpServletResponse response,
                          @PathVariable int id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        writer.println(dao.deleteTicket(id));
    }
    @RequestMapping(value = "/tickets/{id}/flight")
    public void getFlight(HttpServletResponse response,
                             @PathVariable int id) throws IOException {
        Ticket ticket=dao.getTicket(id);
        PrintWriter writer=response.getWriter();
        if(ticket==null){
            writer.println(new Response("ticket not found",null));
            return;
        }
        writer.println(new Response("ok",ticket.getFlight()));
    }
    @RequestMapping(value = "/tickets/{id}/passenger")
    public void getPassenger(HttpServletResponse response,
                          @PathVariable int id) throws IOException {
        Ticket ticket=dao.getTicket(id);
        PrintWriter writer=response.getWriter();
        if(ticket==null){
            writer.println(new Response("ticket not found",null));
            return;
        }
        writer.println(new Response("ok",ticket.getPassenger()));
    }
//    @RequestMapping(value = "/tickets/{id}/passenger",method = RequestMethod.POST)
//    public void setPassenger(HttpServletResponse response,
//                             @PathVariable int id,
//                             @RequestHeader int passenger) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.setPassengerId(ticket,passenger));
//    }
//    @RequestMapping(value = "/tickets/{id}/flight",method = RequestMethod.POST)
//    public void setFlight(HttpServletResponse response,
//                          @PathVariable int id,
//                          @RequestHeader int flight) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.setFlightId(ticket,flight));
//    }
//    @RequestMapping(value = "/tickets/{id}/passenger",method = RequestMethod.DELETE)
//    public void resetPassenger(HttpServletResponse response,
//                             @PathVariable int id) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("passenger not found",null));
//            return;
//        }
//        writer.println(dao.resetPassenger(ticket));
//    }
//    @RequestMapping(value = "/tickets/{id}/flight",method = RequestMethod.DELETE)
//    public void resetFlight(HttpServletResponse response,
//                          @PathVariable int id) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.resetFlight(ticket));
//    }
}
