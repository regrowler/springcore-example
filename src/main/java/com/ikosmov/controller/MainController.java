package com.ikosmov.controller;

import com.ikosmov.dao.AirportDao;
import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Passenger;
import com.ikosmov.entity.Ticket;
import com.ikosmov.utils.ListGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;

@Controller
public class MainController {

    //private static final Log LOG = LogFactory.getLog(MainController.class);
    @Autowired
    AirportDao airportDao;
    @RequestMapping("/")
    public void index(HttpServletResponse response) throws IOException {
       response.sendRedirect("/index.html");
    }
    @RequestMapping("/init")
    public void init() throws IOException{
        Passenger passenger1 = new Passenger(1, "John");
        Passenger passenger2 = new Passenger(2, "Andrew");
        Passenger passenger3 = new Passenger(3, "Michael");
        Passenger passenger5 = new Passenger(4, "DIma");
        Stream.of(passenger1,passenger2,passenger3,passenger5).forEach(airportDao::insertEntity);
        Ticket ticket1 = new Ticket(1, 1, 1);
        Ticket ticket2 = new Ticket(2, 2, 1);
        Ticket ticket3 = new Ticket(3, 3, 1);
        Ticket ticket4 = new Ticket(4, 4, 2);
        Stream.of(ticket1,ticket2,ticket3,ticket4).forEach(airportDao::insertEntity);
        Flight flight1 = new Flight(1, "Samara", "Moscow");
        Flight flight2 = new Flight(2, "Samara", "Saratov");
        Stream.of(flight1, flight2).forEach(airportDao::insertEntity);
        passenger1.setTickets(ListGetter.getListFromEntity(ticket1));
        passenger2.setTickets(ListGetter.getListFromEntity(ticket2));
        passenger3.setTickets(ListGetter.getListFromEntity(ticket3));
        passenger5.setTickets(ListGetter.getListFromEntity(ticket4));
        passenger1.setFlights(ListGetter.getSetFromEntity(flight1,flight2));
        passenger2.setFlights(ListGetter.getSetFromEntity(flight1));
        passenger3.setFlights(ListGetter.getSetFromEntity(flight1));
        passenger5.setFlights(ListGetter.getSetFromEntity(flight2));
        Stream.of(passenger1,passenger2,passenger3,passenger5).forEach(airportDao::mergeEntity);
        ticket1.setPassenger(passenger1);
        ticket1.setFlight(flight1);
        ticket2.setPassenger(passenger2);
        ticket2.setFlight(flight1);
        ticket3.setPassenger(passenger3);
        ticket3.setFlight(flight1);
        ticket4.setPassenger(passenger5);
        ticket4.setFlight(flight2);
        Stream.of(ticket1,ticket2,ticket3,ticket4).forEach(airportDao::mergeEntity);
        flight1.setPassengers(ListGetter.getSetFromEntity(passenger1,passenger2,passenger3));
        flight2.setPassengers(ListGetter.getSetFromEntity(passenger5));
        Stream.of(flight1,flight2).forEach(airportDao::mergeEntity);
    }
}
