package com.ikosmov.controller;

import com.ikosmov.dao.FlightDao;
import com.ikosmov.dao.FlightDao;
import com.ikosmov.entity.Flight;
import com.ikosmov.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

@Controller
public class FlightController {
    @Autowired
    FlightDao dao;
    @RequestMapping("/flights")
    public void getAll(HttpServletResponse response) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        List list= dao.getFlights();
        list.forEach(writer::println);
    }
    @RequestMapping(value = "/flights",method = RequestMethod.POST)
    public void add(HttpServletResponse response,
                    @RequestHeader int id,
                    @RequestHeader int flight,
                    @RequestHeader int passenger) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        dao.getFlights().forEach(writer::println);
    }
    @RequestMapping("/flights/{id}")
    public void getById(HttpServletResponse response, @PathVariable String id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        writer.println(dao.getFlight(Integer.valueOf(id)));
    }
    @RequestMapping(value = "/flights/{id}",method = RequestMethod.POST)
    public void putFlight(HttpServletResponse response,
                          @PathVariable int id,
                          @RequestHeader String from,
                          @RequestHeader String target) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        writer.println(dao.postFlight(id,from,target));
    }
    @RequestMapping(value = "/flights/{id}",method = RequestMethod.DELETE)
    public void deleteFlight(HttpServletResponse response,
                             @PathVariable int id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        writer.println(dao.deleteFlight(id));
    }
    @RequestMapping(value = "/flights/{id}/tickets")
    public void flightTickets(HttpServletResponse response,
                             @PathVariable int id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        Flight flight=dao.getFlight(id);
        List list=dao.getFlightTickets(id);
        if(flight!=null){
            writer.println(new Response("ok",list));
        }else writer.println(new Response("not found",null));
    }
    @RequestMapping(value = "/flights/{id}/passengers")
    public void flightPassengers(HttpServletResponse response,
                             @PathVariable int id) throws IOException {
        int y=0;
        PrintWriter writer=response.getWriter();
        Flight flight=dao.getFlight(id);
        Set set=dao.getFlightPassengers(id);
        if(flight!=null){
            writer.println(new Response("ok",set));
        }else writer.println(new Response("not found",null));
    }
}
