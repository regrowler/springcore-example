package com.ikosmov.dao;

import com.ikosmov.entity.Passenger;
import com.ikosmov.entity.Ticket;
import com.ikosmov.utils.Response;
import com.ikosmov.entity.Flight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class FlightDao {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AirportDao airportDao;
    @Autowired
    TicketDao ticketDao;
    @Autowired
    PassengerDao passengerDao;

    public List<Flight> getFlights() {
        Session sf = sessionFactory.openSession();
        List<Flight> list=AirportDao.getSelectQueryForClass(Flight.class,sf);
        sf.close();
        return list;
    }
    public Flight getFlight(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Flight.class,id,sf);
        Flight flight= (Flight) list.get(0);
        sf.close();
        return flight;
    }
    public List<Ticket> getFlightTickets(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Flight.class,id,sf);
        Flight flight= (Flight) list.get(0);

        List list1=new ArrayList();
        list1.addAll(flight.getTickets());
        sf.close();
        return list1;
    }
    public Set<Passenger> getFlightPassengers(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Flight.class,id,sf);
        Flight flight= (Flight) list.get(0);

        HashSet set=new HashSet();
        set.addAll(flight.getPassengers());
        sf.close();
        return set;
    }
    public Response postFlight(int id, String startPoint, String targetPoint){
        Flight flight=new Flight(id,startPoint,targetPoint);
        try {
            airportDao.insertEntity(flight);
        }catch (Exception e){
            airportDao.mergeEntity(flight);
        }
        return new Response("ok",flight);
    }
    public Response deleteFlight(int id){
        try (Session session=sessionFactory.openSession()){
            Flight flight=getFlight(id);
            if(flight!=null){
                String hql = "delete from Flight where id= :id";
                session.getTransaction().begin();
                session.createQuery(hql).setParameter("id", id).executeUpdate();
                session.getTransaction().commit();
                return new Response("ok",flight);
            }
            return new Response("not found",null);
        }
    }
    public Response addPassenger(int flightId, int id){
        Passenger passenger=passengerDao.getPassenger(id);
        try(Session sf = sessionFactory.openSession();) {
            List list=AirportDao.getSelectQueryForClassById(Flight.class,flightId,sf);
            Flight flight= (Flight) list.get(0);
            if(flight!=null){
                Set set=flight.getPassengers();
                if(set.contains(passenger)){
                    set.remove(passenger);
                    set.add(passenger);
                }else set.add(passenger);
                flight.setPassengers(set);
                airportDao.mergeEntity(flight);
                return new Response("ok",passenger);
            }
            return new Response("flight not found",null);
        }

    }
    public Response removeFlight(Flight flight,int id){
        Passenger passenger=passengerDao.getPassenger(id);
        if(flight!=null){
            Set set=flight.getPassengers();
            if(set.contains(passenger)){
                set.remove(passenger);
            }else set.add(passenger);
            flight.setPassengers(set);
            return new Response("ok",passenger);
        }
        return new Response("flight not found",null);
    }
    public Response removeTicket(Flight flight,int id){
        Ticket ticket=ticketDao.getTicket(id);
        if(ticket!=null){
            List set=flight.getTickets();
            if(set.contains(ticket)){
                set.remove(ticket);
                set.add(ticket);
            }else set.add(ticket);
            flight.setTickets(set);
            return new Response("ok",ticket);
        }
        return new Response("flight not found",null);
    }
    public Response addTicket(int flightId,int id){
        Ticket ticket=ticketDao.getTicket(id);
        try (Session session=sessionFactory.openSession()){
            List list=AirportDao.getSelectQueryForClassById(Flight.class,flightId,session);
            Flight flight= (Flight) list.get(0);
            if(ticket!=null){
                List set=flight.getTickets();
                if(set.contains(ticket)){
                    set.remove(ticket);
                    set.add(ticket);
                }else set.add(ticket);
                flight.setTickets(set);
                airportDao.mergeEntity(flight);
                return new Response("ok",ticket);
            }
            return new Response("flight not found",null);
        }

    }
}
