package com.ikosmov.dao;

import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Passenger;
import com.ikosmov.utils.Response;
import com.ikosmov.entity.Ticket;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TicketDao {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AirportDao airportDao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    PassengerDao passengerDao;

    public List<Ticket> getTickets() {
        Session sf = sessionFactory.openSession();
        List<Ticket> list = AirportDao.getSelectQueryForClass(Ticket.class, sf);
        sf.close();
        return list;
    }

    public Ticket getTicket(int id) {
        Session sf = sessionFactory.openSession();
        List list = AirportDao.getSelectQueryForClassById(Ticket.class, id, sf);
        if(list.size()==0){return null;}
        Ticket ticket = (Ticket) list.get(0);
        sf.close();
        return ticket;
    }

    public Response postTicket(int id, int passenger, int flight) {
        Ticket ticket = getTicket(id);
        Flight flight1;
        Passenger passenger1;
        if (ticket == null) {
            ticket = new Ticket(id, passenger, flight);
            flight1 = flightDao.getFlight(flight);
            passenger1 = passengerDao.getPassenger(passenger);
            if (flight1 == null) {
                return new Response("flight not found", null);

            }
            if (passenger1 == null) {
                return new Response("flight not found", null);
            }
            airportDao.insertEntity(ticket);

        }else {
            flight1 = flightDao.getFlight(flight);
            passenger1 = passengerDao.getPassenger(passenger);
            if (flight1 == null) {
                return new Response("flight not found", null);

            }
            if (passenger1 == null) {
                return new Response("flight not found", null);
            }

        }

        ticket.setFlight(flight1);
        ticket.setPassenger(passenger1);
        airportDao.mergeEntity(ticket);
        flightDao.addPassenger(flight,passenger);
        flightDao.addTicket(flight,id);
        passengerDao.addFlight(passenger,flight);
        passengerDao.addTicket(passenger,id);
        return new Response("ok",ticket);
    }

    public Response deleteTicket(int id) {
        try (Session session = sessionFactory.openSession()) {
            Ticket ticket = getTicket(id);
            if (ticket != null) {
                String hql = "delete from Ticket where id= :id";
                session.getTransaction().begin();
                session.createQuery(hql).setParameter("id", id).executeUpdate();
                session.getTransaction().commit();
                return new Response("ok", ticket);
            }
            return new Response("not found", null);
        }
    }

    public Response setPassengerId(Ticket ticket, int id) {
        ticket.setPassengerId(id);
        if (passengerDao.getPassenger(id) != null) {
            ticket.setPassenger(passengerDao.getPassenger(id));
        }
        airportDao.mergeEntity(ticket);
        return new Response("ok", ticket);
    }

    public Response resetPassenger(Ticket ticket) {
        ticket.setPassenger(null);
        ticket.setPassengerId(0);
        airportDao.mergeEntity(ticket);
        return new Response("ok", ticket);
    }

    public Response setFlightId(Ticket ticket, int id) {
        ticket.setPassengerId(id);
        if (flightDao.getFlight(id) != null) {
            ticket.setFlight(flightDao.getFlight(id));
        }
        airportDao.mergeEntity(ticket);
        return new Response("ok", ticket);
    }

    public Response resetFlight(Ticket ticket) {
        ticket.setFlight(null);
        ticket.setFlightId(0);
        airportDao.mergeEntity(ticket);
        return new Response("ok", ticket);
    }
}
