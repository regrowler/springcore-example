package com.ikosmov.dao;

import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Ticket;
import com.ikosmov.utils.Response;
import com.ikosmov.entity.Passenger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class PassengerDao {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AirportDao airportDao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    TicketDao ticketDao;

    public List<Passenger> getPassengers() {
        Session sf = sessionFactory.openSession();
        List<Passenger> list=AirportDao.getSelectQueryForClass(Passenger.class,sf);
        sf.close();
        return list;
    }
    public Passenger getPassenger(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Passenger.class,id,sf);
        Passenger Passenger= (Passenger) list.get(0);
        sf.close();
        return Passenger;
    }
    public Response postPassenger(int id, String name){
        Passenger passenger=new Passenger(id,name);
        try {
            airportDao.insertEntity(passenger);
        }catch (Exception e){
            airportDao.mergeEntity(passenger);
        }
        return new Response("ok",passenger);
    }
    public Response postPassenger(String name){
        Session session=sessionFactory.openSession();
        Passenger passenger=new Passenger(airportDao.getLastidInTable(Passenger.class,session)+1,name);
        try {
            airportDao.insertEntity(passenger);
        }catch (Exception e){
            airportDao.mergeEntity(passenger);
        }
        session.close();
        return new Response("ok",passenger);
    }
    public Response deletePassenger(int id){
        try (Session session=sessionFactory.openSession()){
            Passenger Passenger=getPassenger(id);
            if(Passenger!=null){
                String hql = "delete from Passenger where id= :id";
                session.getTransaction().begin();
                session.createQuery(hql).setParameter("id", id).executeUpdate();
                session.getTransaction().commit();
                return new Response("ok",Passenger);
            }
            return new Response("not found",null);
        }
    }
    public Response addFlight(int passengerId,int id){
        Flight flight=flightDao.getFlight(id);
        try (Session session=sessionFactory.openSession()){
            List list=AirportDao.getSelectQueryForClassById(Passenger.class,passengerId,session);
            Passenger passenger= (Passenger) list.get(0);
            if(flight!=null){
                Set set=passenger.getFlights();
                if(set.contains(flight)){
                    set.remove(flight);
                }else set.add(flight);
                passenger.setFlights(set);
                airportDao.mergeEntity(passenger);
                return new Response("ok",passenger);
            }
            return new Response("flight not found",null);
        }
    }
    public Response removeFlight(Passenger passenger,int id){
        Flight flight=flightDao.getFlight(id);
        if(flight!=null){
            Set set=passenger.getFlights();
            if(set.contains(flight)){
                set.remove(flight);
            }else set.add(flight);
            passenger.setFlights(set);
            return new Response("ok",flight);
        }
        return new Response("flight not found",null);
    }
    public Response removeTicket(Passenger passenger,int id){
        Ticket ticket=ticketDao.getTicket(id);
        if(ticket!=null){
            List set=passenger.getTickets();
            if(set.contains(ticket)){
                set.remove(ticket);
            }else set.add(ticket);
            passenger.setTickets(set);
            return new Response("ok",ticket);
        }
        return new Response("flight not found",null);
    }
    public Response addTicket(int passengerId,int id){
        Ticket ticket=ticketDao.getTicket(id);
        try (Session session=sessionFactory.openSession()){
            List list=AirportDao.getSelectQueryForClassById(Passenger.class,passengerId,session);
            Passenger passengert= (Passenger) list.get(0);
            if(ticket!=null){
                List set=passengert.getTickets();
                if(set.contains(ticket)){
                    set.remove(ticket);
                    set.add(ticket);
                }else set.add(ticket);
                passengert.setTickets(set);
                airportDao.mergeEntity(passengert);
                return new Response("ok",ticket);
            }
            return new Response("flight not found",null);
        }
    }
    public List<Ticket> getPassengersTickets(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Passenger.class,id,sf);
        Passenger flight= (Passenger) list.get(0);

        List list1=new ArrayList();
        list1.addAll(flight.getTickets());
        sf.close();
        return list1;
    }
    public Set<Passenger> getPassengersFlights(int id){
        Session sf = sessionFactory.openSession();
        List list=AirportDao.getSelectQueryForClassById(Passenger.class,id,sf);
        Passenger flight= (Passenger) list.get(0);

        HashSet set=new HashSet();
        set.addAll(flight.getFlights());
        sf.close();
        return set;
    }

}
