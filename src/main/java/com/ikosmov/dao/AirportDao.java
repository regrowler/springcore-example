package com.ikosmov.dao;

import com.ikosmov.entity.AirportEntity;
import com.ikosmov.entity.Flight;
import com.ikosmov.entity.Passenger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AirportDao {
    @Autowired
    SessionFactory sessionFactory;
    public final String sqlJoin = "select t.id,f.startPoint,f.targetPoint,p.name from Flight f,Passenger p,Ticket t " +
            "where t.passengerId=p.id and t.flightId=f.id";
    public final String sqlOrderBy = "select p.name from Passenger p order by p.id desc";

    public void insertEntity(AirportEntity entity) {
        Session sf = sessionFactory.openSession();
        sf.getTransaction().begin();
        sf.persist(entity);
        sf.getTransaction().commit();
        sf.close();
    }

    public void mergeEntity(AirportEntity entity) {
        Session sf = sessionFactory.openSession();
        sf.getTransaction().begin();
        sf.merge(entity);
        sf.getTransaction().commit();
        sf.close();

    }

    public static List getSelectQueryForClass(Class c,Session session){
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(c);
        Root ticketRoot = query.from(c);
        return session.createQuery(query).getResultList();
    }
    public static  List getSelectQueryForClassById(Class c,int id,Session session){
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(c);
        Root ticketRoot = query.from(c);
        query.select(ticketRoot).where(builder.equal(ticketRoot.get("id"),id));
        return session.createQuery(query).getResultList();
    }
    public  int getLastidInTable(Class c,Session session){
        List<AirportEntity> list=getSelectQueryForClass(c,session);
        return list.get(list.size()-1).getId();
    }





}
