package com.ikosmov.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@NamedQuery(name = "PassengerEntity.orderBy",query = "select p.name from Passenger p order by p.id desc")
public class Passenger implements AirportEntity {
    public Passenger(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    private int id;

    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "passenger")
    private List<Ticket> tickets;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "passengers_flihts",
            joinColumns = @JoinColumn(name = "fligt_id"),
            inverseJoinColumns = @JoinColumn(name = "passenger_id")
    )
    private Set<Flight> flights;

    public Passenger() {
    }


    public int getId() {
        return id;
    }

    public void setId(int passenger_id) {
        this.id = passenger_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> entity) {
        this.tickets = entity;
    }

    public Set<Flight> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flight> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Passenger{" +
                "id=" + id +
                ", name='" + name + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return id == passenger.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
