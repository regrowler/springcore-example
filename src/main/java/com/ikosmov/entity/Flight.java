package com.ikosmov.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Flight implements AirportEntity {
    public Flight(int id, String startPoint, String targetPoint) {
        this.id = id;
        this.startPoint = startPoint;
        this.targetPoint = targetPoint;
    }

    @Id
    private int id;
    public String startPoint;
    public String targetPoint;

    @JsonIgnore
    @OneToMany(mappedBy = "flight")
    private List<Ticket> tickets;
    @JsonIgnore
    @ManyToMany
    private Set<Passenger> passengers;

    public Flight() {
    }

    public int getId() {
        return id;
    }

    public void setId(int flight_id) {
        this.id = flight_id;
    }

    public String getstartPoint() {
        return startPoint;
    }

    public void setstartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String gettargetPoint() {
        return targetPoint;
    }

    public void settargetPoint(String targetPoint) {
        this.targetPoint = targetPoint;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Set<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(Set<Passenger> passengers) {
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Flight{" +
                "id=" + id +
                ", startPoint='" + startPoint + '\'' +
                ", targetPoint='" + targetPoint + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return id == flight.id;
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
