package com.ikosmov.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@Entity
@NamedQuery(name = "TicketEntity.joinAll", query = "select t.id,f.startPoint,f.targetPoint,p.name from Flight f,Passenger p,Ticket t where t.passengerId=p.id and t.flightId=f.id")
public class Ticket implements AirportEntity {
    public Ticket(int id, int passengerId, int flightId) {
        this.id = id;
        this.passengerId = passengerId;
        this.flightId = flightId;
    }

    @Id
    private int id;
    public int passengerId;
    private int flightId;
    @JsonIgnore
    @ManyToOne
    private Passenger passenger;
    @JsonIgnore
    @ManyToOne
    private Flight flight;

    public Ticket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int ticket_id) {
        this.id = ticket_id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flight_id) {
        this.flightId = flight_id;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(int passenger_id) {
        this.passengerId = passenger_id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Ticket{" +
                "id=" + id +
                ", passengerId=" + passengerId +
                ", flightId=" + flightId +
                ", passenger=" + passenger +
                ", flight=" + flight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id == ticket.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
