package com.ikosmov.utils;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;


import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class HibernateConf {
    public static final String postresUrl="ikosmov.hldns.ru";
    @Bean
    @Profile("dev")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(
                "com.ikosmov.entity" );
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }


    @Bean
    @Profile("prod")
    public LocalSessionFactoryBean sessionFactoryProd() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(postgresDataSource());
        sessionFactory.setPackagesToScan(
                "com.ikosmov.entity" );
        sessionFactory.setHibernateProperties(hibernatePostgresProperties());

        return sessionFactory;
    }
    public DataSource postgresDataSource() {

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://"+postresUrl+":5432/ttt?createDatabaseIfNotExist=true");
        dataSource.setUsername("postgres");
        dataSource.setPassword("123");

        return dataSource;
    }
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        return dataSource;
    }

    @Bean
    @Profile("dev")
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager
                = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    @Profile("prod")
    public PlatformTransactionManager hibernateTransactionManagerProd() {
        HibernateTransactionManager transactionManager
                = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactoryProd().getObject());
        return transactionManager;
    }
    private final Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.setProperty(
                "hibernate.dialect", "org.hibernate.dialect.H2Dialect");

        return hibernateProperties;
    }

    private final Properties hibernatePostgresProperties() {
        Properties hibernateProperties = new Properties();

        hibernateProperties.setProperty(
                "hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect");
        hibernateProperties.getProperty("hibernate.c3p0.min_size","5");
        hibernateProperties.getProperty("hibernate.c3p0.max_size","20");
        hibernateProperties.getProperty("hibernate.c3p0.timeout","300");
        return hibernateProperties;
    }
}
